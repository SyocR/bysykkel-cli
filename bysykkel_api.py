from enum import Enum
import requests
from fastapi import FastAPI, HTTPException, Response

app = FastAPI(title="Bysykkel API", version="0.0.1")

class AvailableType(str, Enum):
    num_bikes_available = "bikes"
    num_docks_available = "docks"

# Very simple requests wrapper
def do_get(endpoint):
    url = f"http://gbfs.urbansharing.com/oslobysykkel.no/{endpoint}.json"
    r = requests.get(url, headers={"Client-Identifier": "vinjar-kodeoppgave"})
    if r.status_code != 200:
        raise HTTPException(
                status_code=500,
                detail=f"Failed to reach upstream API: {r.text}"
                )
    return r.json()

def calculate_stats(status):
    docks = bikes = 0
    for station in status:
        docks += station["num_docks_available"]
        bikes += station["num_bikes_available"]
    count = len(status)
    avg_docks = docks / len(status)
    avg_bikes = bikes / len(status)
    return {
            "count": count,
            "docks": docks,
            "bikes": bikes,
            "avg_docks": round(avg_docks, 2),
            "avg_bikes": round(avg_bikes, 2)
            }

def merge_station_info():
    information = do_get("station_information")["data"]["stations"]
    status = do_get("station_status")["data"]["stations"]

    # Merge the two dicts and key dict by station ID
    stations = {x["station_id"]: x for x in information} # transpose stations list
    # Enrich stations with status info
    for station in status:
        s_id = station["station_id"]

        # merge status dict into station
        stations[s_id] = {**stations[s_id], **station}
    return stations

@app.get("/api/v1/stations")
async def get_stations():
    return merge_station_info()

@app.get("/api/v1/stats")
async def get_stats():
    status = do_get("station_status")["data"]["stations"]
    return calculate_stats(status)

@app.get("/api/v1/available/{available_type}")
async def get_available(available_type: AvailableType):
    stations = merge_station_info()
    # Filter for either available docks or bikes
    available_stations = {k: v for k, v in stations.items() if v[available_type.name]}
    return available_stations

@app.get("/api/v1/station/{station}")
async def get_station(station: int):
    station = str(station)
    try:
        res = merge_station_info()[station]
    except KeyError:
        raise HTTPException(
                status_code=404,
                detail=f"Unable to find station with ID {station}"
                )
    return res

@app.get("/api/v1/health")
async def healthy():
    return Response(content="Ok.")
