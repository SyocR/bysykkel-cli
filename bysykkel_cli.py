import requests
from rich.table import Table
from textual.app import App
from textual import events
from textual.widgets import Header, Footer, ScrollView

class Bikes(App):
    async def on_load(self, event: events.Load):
        await self.bind("b", "view.toggle('sidebar')", "Toggle sidebar")
        await self.bind("q", "quit", "Quit")
        await self.bind("r", "refresh()", "refresh")
        await self.bind("escape", "quit", "Quit")

    # Create tables for displaying and update them with dynamic content
    def init_tables(self):
        self.station_table = Table(title="Station list")
        self.station_table.add_column("ID")
        self.station_table.add_column("Name")
        self.station_table.add_column("Available Bikes")
        self.station_table.add_column("Available Docs")

        self.stats_table = Table(title="Stats")
        self.stats_table.add_column("Total number of stations")
        self.stats_table.add_column("Total docks")
        self.stats_table.add_column("Total bikes")
        self.stats_table.add_column("Average available docks")
        self.stats_table.add_column("Average available bikes")

        r = requests.get("http://localhost:8000/api/v1/stats")
        stats = r.json()
        self.stats_table.add_row(
                str(stats["count"]),
                str(stats["docks"]),
                str(stats["bikes"]),
                str(stats["avg_docks"]),
                str(stats["avg_bikes"])
                )

        r = requests.get("http://localhost:8000/api/v1/stations")
        stations = r.json()
        for station in stations.values():
            self.station_table.add_row(
                    str(station['station_id']),
                    station['name'],
                    str(station['num_bikes_available']),
                    str(station['num_docks_available'])
                    )

    # Ran when user presses "r"
    async def action_refresh(self):
        success = self.init_tables()

        # update views
        if success:
            await self.body.update(self.station_table)
            await self.top_stats.update(self.stats_table)

    async def on_mount(self, event):
        self.body = body = ScrollView(auto_width=True)
        self.top_stats = top_stats = ScrollView(auto_width=True)

        await self.view.dock(Header(), edge="top")
        await self.view.dock(Footer(), edge="bottom")
        await self.view.dock(top_stats, edge="top", size=10, name="sidebar")
        await self.view.dock(body)

        self.init_tables()

        async def update_table():

            # update views
            await body.update(self.station_table)
            await top_stats.update(self.stats_table)

        await self.call_later(update_table)
