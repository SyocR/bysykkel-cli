import json
from pathlib import Path
from unittest.mock import patch
import requests
from bysykkel_cli import Bikes
from rich.table import Table

# fake requests.get function for serving static data
def fake_get(path):
    class FakeResponse():
        def __init__(self, data):
            self.data = data
        def json(self):
            return self.data

    if path.endswith('stations'):
        with Path('tests/stations.json').open('r') as f:
            return FakeResponse(json.load(f))
    else:
        with Path('tests/stats.json').open('r') as f:
            return FakeResponse(json.load(f))

def init_stats_table():
    t = Table(title="Stats")
    t.add_column("Total number of stations")
    t.add_column("Total docks")
    t.add_column("Total bikes")
    t.add_column("Average available docks")
    t.add_column("Average available bikes")

    return t

@patch('requests.get', fake_get)
def test_stats_table():
    b = Bikes()
    b.init_tables()
    assert b.stats_table.title == "Stats"
    assert len(b.stats_table.columns) == 5
    assert b.stats_table.row_count == 1

@patch('requests.get', fake_get)
def test_station_table():
    b = Bikes()
    b.init_tables()
    assert b.station_table.title == "Station list"
    assert len(b.station_table.columns) == 4
    assert b.station_table.row_count == 259
