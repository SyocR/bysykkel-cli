import json
from pathlib import Path
from unittest.mock import patch
from fastapi.testclient import TestClient
from bysykkel_api import app, do_get

client = TestClient(app)

# Patch out dynamic data
def fake_get(endpoint):
    with Path(f'tests/{endpoint}.json').open('r') as f:
        return json.load(f)

def test_health():
    r = client.get("/api/v1/health")
    assert r.text == "Ok."

def test_wrong_endpoint():
    r = client.get("/api/v1/not_implemented")
    assert r.status_code == 404

@patch('bysykkel_api.do_get', fake_get)
def test_stats():
    r = client.get("/api/v1/stats")
    res = r.json()
    assert res["count"] == 259
    assert res["bikes"] == 1910
    assert res["docks"] == 3790
    assert res["avg_bikes"] == 7.37
    assert res["avg_docks"] == 14.63

    assert sorted(res.keys()) == [
            "avg_bikes",
            "avg_docks",
            "bikes",
            "count",
            "docks"
            ]

@patch('bysykkel_api.do_get', fake_get)
def test_stations():
    r = client.get("/api/v1/stations")
    res = r.json()

    assert len(res.keys()) == 259
    assert "2350" in res.keys()
    assert "1337" not in res.keys()
    assert res["485"]["name"] == "Sommerfrydhagen"

    for i in res.keys():
        assert res[i]["station_id"] == i

@patch('bysykkel_api.do_get', fake_get)
def test_available():
    r = client.get("/api/v1/available/docks")
    docks = r.json()
    assert len(docks.keys()) == 249

    r = client.get("/api/v1/available/bikes")
    bikes = r.json()
    assert len(bikes.keys()) == 206

@patch('bysykkel_api.do_get', fake_get)
def test_station():
    r = client.get("/api/v1/station/455")
    res = r.json()
    assert res == {
            "station_id": "455",
            "name": "Sofienbergparken sør",
            "address": "Sofienberggata 17",
            "rental_uris": {
                "android": "oslobysykkel://stations/455",
                "ios": "oslobysykkel://stations/455"
                },
            "lat": 59.922337,
            "lon": 10.7617503,
            "capacity": 12,
            "is_installed": 1,
            "is_renting": 1,
            "is_returning": 1,
            "last_reported": 1663686812,
            "num_bikes_available": 6,
            "num_docks_available": 6
            }

    r = client.get("/api/v1/station/1337")
    assert r.status_code == 404

def test_dynamic_data():
    r = client.get("/api/v1/stations")
    assert r.status_code == 200
    r = client.get("/api/v1/stats")
    assert r.status_code == 200
