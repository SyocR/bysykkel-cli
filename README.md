# Simple CLI for Bysykkel stations

Made possible due to [Textual](https://github.com/Textualize/textual)  
Contains a simple API for easier access to upstream data  

## Requirements

`$ python3 -m pip install requests textual fastapi uvicorn`

## Usage
Clone the repo, start the API and run the executable.  

    $ uvicorn bysykkel_api:app
    $ ./bysykkel-cli

Use ESC or Q to quit. B toggles the stats panel and R refreshes the data.

## Testing

Run using pytest in repo root directory.  

    $ python3 -m pytest
